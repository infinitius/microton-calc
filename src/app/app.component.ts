import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HistoryDialogComponent } from './history-dialog/history-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Microton Calculator';
  numbers: string[] = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0'];
  subText: string = '';
  mainText: string = '';
  history: string[] = [];

  constructor(public dialog: MatDialog) {}

  allClear() {
    this.mainText = '';
    this.subText = '';
    this.history.push('Vynulováno');
  }

  isSpecialKey(key: string): boolean {
    return (key === '/' || key === '*' || key === '-' || key === '+' || key === '.') ? true : false;
  }

  pressKey(key: string) {
    if (this.isSpecialKey(key)) {
      const lastKey = this.mainText[this.mainText.length - 1];
      if (this.isSpecialKey(lastKey)) {
        this.mainText = this.mainText.slice(0, this.mainText.length - 1);
      }
    }
    if (key === '.' && this.mainText.includes('.')) {
      return;
    }
    this.mainText += key;
    return;
  }

  getResult() {
    try {
      this.subText = this.mainText;
      this.mainText = eval(this.mainText).toString();
      this.history.push(`${this.subText} = ${this.mainText}`);
    } catch (e) {
      alert('Nejde vyhodnotit výraz');
    }
  }

  openHistoryDialog(): void {
    this.dialog.open(HistoryDialogComponent, {
      width: '500px',
      data: this.history
    });
  }
}
