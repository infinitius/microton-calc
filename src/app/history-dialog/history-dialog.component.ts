import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-history-dialog',
  templateUrl: 'history-dialog.component.html'
})

export class HistoryDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<HistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string[]) {}

    onClose(): void {
        this.dialogRef.close();
    }
}
